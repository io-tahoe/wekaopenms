/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.core;

/**
 *
 * @author alexmy
 */
public interface Consts {
    // WekaHttpClient
    public static String HZ_JSON = "hz_json";
    public static String INIT_PROPS = "init.properties";
    public static String HOST_URL_UPLOAD = "host_url_upload";
    public static String HOST_URL_PROCESS = "host_url_process";
    public static String CLIENT_FILE_PATH = "client_file_path";
    
    // WekaHttpServer
    public static String FILE_NAME = "filename";
    public static String UPLOAD_FOLDER = "upload_folder";
    public static String PORT = "port";
    
    // Weka Processor
    public static String NUM_OF_FOLDS = "num_of_folds";
    public static String CLASSIFIER_LIST = "classifier_list";
    public static String LIST_STRING_DEL = "list_string_del";
    public static String J48 = "J48";
    public static String PART = "PART";
    public static String DECISION_TABLE = "DecisionTable";
    public static String DECISION_STUMP = "DecisionStump";
    
}