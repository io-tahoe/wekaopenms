/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.httpserver.data;

import com.google.gson.Gson;
import java.util.Map;

/**
 *
 * @author alexmy
 */
public class WekaResultImpl implements WekaResult {

    Map<String, Double> resultMap;
    
    /**
     * 
     * @return 
     */
    @Override
    public Map<String, Double> getResultMap() {
        return resultMap;
    }

    /**
     * 
     * @param key
     * @return 
     */
    @Override
    public Double getResultEntry(String key) {       
        return resultMap == null ? null : resultMap.get(key);
    }

    /**
     * 
     * @param key
     * @param value
     * @return 
     */
    @Override
    public boolean addResultEntry(String key, Double value) {
        if(resultMap == null || resultMap.containsKey(key)) {
            return false;
        } else {
            resultMap.put(key, value);
            return true;
        }
    }    

    /**
     *
     * @return
     */
    @Override
    public String toJson() {
        return (String) new Gson().toJson(resultMap, Map.class);
    }
}
