/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.httpserver.processor;

import com.google.gson.Gson;
import com.rokittech.weka.core.Consts;
import com.rokittech.weka.httpserver.data.WekaResult;
import com.rokittech.weka.httpserver.data.WekaResultImpl;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.core.FastVector;
import weka.core.Instances;

/**
 * The WekaTest example from
 *
 * http://www.programcreek.com/2013/01/a-simple-machine-learning-example-in-java/
 *
 * was used as a start point for this Program
 *
 * @author alexmy
 */
public class WekaProcessor {

    int defaultSplits = 10;

    public void process(Properties props) {

        FileReader datareader = null;
        WekaResult wekaResult = new WekaResultImpl();

        try {
            datareader = new FileReader(props.getProperty(Consts.FILE_NAME));
            BufferedReader datafile = readData(datareader);

            Instances data = new Instances(datafile);
            data.setClassIndex(data.numAttributes() - 1);

            // Do N-split cross validation
            String splitsStr = (String) props.get(Consts.NUM_OF_FOLDS);
            int splits = splitsStr == null ? defaultSplits : Integer.valueOf(splitsStr);

            Instances[][] split = crossValidationSplit(data, splits);

            // Separate split into training and testing arrays
            Instances[] trainingSplits = split[0];
            Instances[] testingSplits = split[1];

            Classifier[] models = getClassifiers(props.getProperty(Consts.CLASSIFIER_LIST));

            // Run for each model
            for (Classifier model : models) {

                double accuracy = processModel(model, trainingSplits, testingSplits);
                if (wekaResult.addResultEntry(model.getClass().getSimpleName(), accuracy)) {
                    // Supposed to move Weka ML results to the persistent storage increamentally
                    // one model at the time
                    persist(props, wekaResult.toJson());
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WekaProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WekaProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(WekaProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (datareader != null) {
                    datareader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(WekaProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     *
     * @param model
     * @param trainingSplits
     * @param testingSplits
     * @return
     * @throws Exception
     */
    private double processModel(Classifier model, Instances[] trainingSplits, Instances[] testingSplits) throws Exception {

        // Collect every group of predictions for current model in a FastVector
        FastVector predictions = new FastVector();
        // For each training-testing split pair, train and test the classifier
        for (int i = 0; i < trainingSplits.length; i++) {
            Evaluation validation = classify(model, trainingSplits[i], testingSplits[i]);
            predictions.appendElements(validation.predictions());
//            Uncomment to see the summary for each training-testing pair.
//            System.out.println(model.toString());
        }
        // Calculate overall accuracy of current classifier on all splits
        double accuracy = calculateAccuracy(predictions);
        // Print current classifier's name and accuracy in a complicated,
        // but nice-looking way.
        System.out.println("Accuracy of "
                + model.getClass().getSimpleName() + ": "
                + String.format("%.2f%%", accuracy)
                + "\n---------------------------------");
        return accuracy;
    }

    /**
     *
     * @param datareader
     * @return
     */
    private BufferedReader readData(FileReader datareader) {

        ClassLoader classLoader = getClass().getClassLoader();
        BufferedReader inputReader = new BufferedReader(datareader);

        return inputReader;
    }

    /**
     *
     * @param model
     * @param trainingSet
     * @param testingSet
     * @return
     * @throws Exception
     */
    private Evaluation classify(Classifier model, Instances trainingSet, Instances testingSet) throws Exception {

        Evaluation evaluation = new Evaluation(trainingSet);
        model.buildClassifier(trainingSet);
        evaluation.evaluateModel(model, testingSet);

        return evaluation;
    }

    /**
     *
     * @param predictions
     * @return
     */
    private double calculateAccuracy(FastVector predictions) {
        double correct = 0;

        for (int i = 0; i < predictions.size(); i++) {
            NominalPrediction np = (NominalPrediction) predictions.elementAt(i);
            if (np.predicted() == np.actual()) {
                correct++;
            }
        }
        return 100 * correct / predictions.size();
    }

    /**
     *
     * @param data
     * @param numberOfFolds
     * @return
     */
    private Instances[][] crossValidationSplit(Instances data, int numberOfFolds) {
        Instances[][] split = new Instances[2][numberOfFolds];

        for (int i = 0; i < numberOfFolds; i++) {
            split[0][i] = data.trainCV(numberOfFolds, i);
            split[1][i] = data.testCV(numberOfFolds, i);
        }
        return split;
    }

    /**
     *
     * @param props
     * @param json
     */
    private void persist(Properties props, String json) {
        // TODO. Implement persistence

    }

    /**
     *
     * @param classList
     * @return
     */
    public Classifier[] getClassifiers(String classList) {

        if (classList == null) {
            // Use a default set of classifiers            
            return new WekaClassifierRepo().getAllClassifiers();
        } else {
            // Extract classifiers from classList JSON string
            List<String> list = new Gson().fromJson(classList, List.class);
            List<Classifier> clsList = new ArrayList<>();
            if (list.size() > 0) {
                for (String name : list) {
                    switch (name) {
                        case Consts.J48:
                            clsList.add(new J48());
                            break;
                        case Consts.PART:
                            clsList.add(new PART());
                            break;
                        case Consts.DECISION_STUMP:
                            clsList.add(new DecisionStump());
                            break;
                        case Consts.DECISION_TABLE:
                            clsList.add(new DecisionTable());
                            break;
                        default:
                            break;
                    }
                }
                return clsList.toArray(new Classifier[clsList.size()]);
            } else {
                return null;
            }
        }
    }
}
