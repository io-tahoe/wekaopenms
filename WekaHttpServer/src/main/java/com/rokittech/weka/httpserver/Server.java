/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.httpserver;

import com.rokittech.weka.core.Consts;
import io.vertx.core.AbstractVerticle;
//import io.vertx.example.util.Runner;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Server class is a modified version of the Vert.x example
 * https://github.com/vert-x3/vertx-examples/tree/master/web-examples/src/main/java/io/vertx/example/web/upload
 *
 * @author alexmy
 */
public class Server extends AbstractVerticle {

    Properties props;
    // Convenience method so you can run it in your IDE
    public static void main(String[] args) {
        Runner.runVerticle(Server.class);
    }

    @Override
    public void start() throws Exception {

        props = new Properties();
        try {
            props.load(getClass().getClassLoader().getResourceAsStream(Consts.INIT_PROPS));
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        Router router = Router.router(vertx);

        // Enable multipart form data parsing
        router.route().handler(BodyHandler.create().setUploadsDirectory(props.getProperty(Consts.UPLOAD_FOLDER)));

        // handle the upload
        router.post("/upload").blockingHandler(ctx -> {
            new Controller().uploadHandler(ctx);
            ctx.response().end();
        });

        // handle the ML processing
        router.post("/process").blockingHandler(ctx -> {
            new Controller().processHandler(ctx, props);
            ctx.response().end();
        });

        vertx.createHttpServer().requestHandler(router::accept).listen(Integer.valueOf(props.getProperty(Consts.PORT)));
    }

}
