/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.httpserver;

import co.paralleluniverse.fibers.Suspendable;
import com.rokittech.weka.core.Consts;
import com.rokittech.weka.httpserver.processor.WekaProcessor;
import io.vertx.ext.sync.SyncVerticle;
import io.vertx.ext.web.RoutingContext;
import java.util.Properties;

/**
 *
 * @author alexmy
 */
public class Controller extends SyncVerticle {

    /**
     *
     * @param ctx
     */
    @Suspendable
    public void uploadHandler(RoutingContext ctx) {

        ctx.response().putHeader("Content-Type", "text/plain");

        ctx.response().setChunked(true);

        ctx.fileUploads().stream().map((file) -> {
            System.out.println("f");
            return file;
        }).map((file) -> {
            ctx.response().write("Filename: " + file.fileName());
            return file;
        }).forEachOrdered((file) -> {
            ctx.response().putHeader(Consts.FILE_NAME, file.uploadedFileName())
                    .write("\n")
                    .write("File Name: " + file.uploadedFileName() + "; Size: " + file.size());
        });
    }

    /**
     *
     * @param ctx
     * @param props
     */
    @Suspendable
    public void processHandler(RoutingContext ctx, Properties props) {

        ctx.response().putHeader("Content-Type", "text/plain");
        ctx.response().setChunked(true);

        String fileName = ctx.request().getHeader(Consts.FILE_NAME);

        if (fileName != null) {
            props.put(Consts.FILE_NAME, fileName);
            // Collect rest of the properties
            collectProps(ctx, props);

            new WekaProcessor().process(props);

            ctx.response().write("File processed.");
        } else {
            ctx.response().write("ERROR: File not found!");
        }
    }

    public void collectProps(RoutingContext ctx, Properties props) {

        String modelList = ctx.request().getHeader(Consts.CLASSIFIER_LIST);
        if (modelList != null) {
            props.setProperty(Consts.CLASSIFIER_LIST, modelList);
        }

        String numOfFolds = ctx.request().getHeader(Consts.NUM_OF_FOLDS);
        if (numOfFolds != null) {
            props.setProperty(Consts.NUM_OF_FOLDS, numOfFolds);
        }

        String hzJson = ctx.request().getHeader(Consts.HZ_JSON);
        if (hzJson != null) {
            props.put(Consts.HZ_JSON, hzJson);
            System.out.println("HZ JSON: " + hzJson);
        }
    }
}
