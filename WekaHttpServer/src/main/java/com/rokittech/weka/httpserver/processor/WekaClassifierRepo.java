/*
 * Copyright 2017 ROKITT Inc.
 * (https://www.rokittastra.com)

 * This program is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * This program also uses Vert.x software that is licensed under 
 * the Apache License, Version 2.0 (the "License");
 * you may not use Vert.x files except in compliance with the License.
 *
 * We are using Vert.x software with Apache 2 license according to the
 * recommendations of ASF:
 * 
 *      https://www.apache.org/licenses/GPL-compatibility.html
 * 
 * You may obtain a copy of the Apache 2 License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License and Apache 2 License for more details.
 *
 */
package com.rokittech.weka.httpserver.processor;

import com.rokittech.weka.core.Consts;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import weka.classifiers.Classifier;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;

/**
 *
 * @author alexmy
 */
public class WekaClassifierRepo {
    
    private HashMap<String, Classifier> repo;
    
    public WekaClassifierRepo() {
        initRepo();        
    }

    /**
     * Creates initial Classifier's repo
     */
    private void initRepo() {
        
        if(repo == null) {
            repo = new HashMap<>();
        }
        
        repo.put(Consts.J48, new J48());
        repo.put(Consts.DECISION_TABLE, new DecisionTable());
        repo.put(Consts.PART, new PART());
        repo.put(Consts.DECISION_STUMP, new DecisionStump());
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public Classifier getClassifier(String name) {
        return repo.get(name);
    }
    
    /**
     * 
     * @return 
     */
    public Map<String, Classifier> getClassifierMap(){
        return repo;
    }
    
    /**
     * 
     * @param name
     * @param classifier
     * @return 
     */
    public Boolean addClassifier(String name, Classifier classifier) {
        Boolean success = true;
        
        if(repo.containsKey(name)) {
            success = false;
        } else {
            repo.put(name, classifier);
        }        
        return success;
    }
    
    /**
     * 
     * @return 
     */
    public Classifier[] getAllClassifiers(){
        Collection<Classifier> values = repo.values();
        return values.toArray(new Classifier[values.size()]);
    }
}
