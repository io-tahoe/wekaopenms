/*
 * This is a slightly modified version of the code presented on
 * 
 *      https://hc.apache.org/httpcomponents-client-ga/httpmime/examples/org/apache/http/examples/entity/mime/ClientMultipartFormPost.java
 * 
 * The changes include a replacement of the original package name 
 * 
 *      org.apache.http.examples.client
 *
 * with the name that complys with the 
 * ROKITT company standard naming convention.
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */
package com.rokittech.weka.httpclient;

import com.google.gson.Gson;
import com.rokittech.weka.core.Consts;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author alexmy
 */
public class WekaHttpClient {

    public static void main(String[] args) throws Exception {

        WekaHttpClient client = new WekaHttpClient();

        Properties props = new Properties();
         try {
            props.load(client.getClass().getClassLoader().getResourceAsStream(Consts.INIT_PROPS));
        } catch (IOException ex) {
            Logger.getLogger(WekaHttpClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        callWekaHttpServer(client, props);
    }

    /**
     * 
     * @param client
     * @param props
     * @throws IOException
     * @throws ParseException 
     */
    public static void callWekaHttpServer(WekaHttpClient client, Properties props) throws IOException, ParseException {
        
        CloseableHttpClient httpclient = HttpClients.createDefault();
        
        int uploadStatus = client.uploadReq(httpclient, props);
        
        if (uploadStatus == HttpStatus.SC_OK) {
            
            int processStatus = client.processReq(httpclient, props);

            switch (processStatus) {
                case HttpStatus.SC_OK:
                    System.out.println("ML processing is completed with status code: " + processStatus);

                    break;
                default:
                    System.out.println("ML processing is failed with status code: " + processStatus);
                    break;
            }
        } else {
            System.out.println("File upload is failed with status code: " + uploadStatus);
        }
    }

    /**
     *
     * @param httpclient
     * @param props
     * @return
     * @throws ParseException
     * @throws IOException
     */
    public synchronized int uploadReq(CloseableHttpClient httpclient, Properties props) throws ParseException, IOException {

        HttpPost httppost = new HttpPost(props.getProperty(Consts.HOST_URL_UPLOAD));

        FileBody txt = new FileBody(new File(props.getProperty(Consts.CLIENT_FILE_PATH))); 
        
        StringBody comment = new StringBody("A text file with data for Weka ML.", ContentType.TEXT_PLAIN);

        HttpEntity reqEntity = MultipartEntityBuilder.create()
                .addPart("txt", txt)
                .addPart("comment", comment)
                .build();

        httppost.setEntity(reqEntity);

        System.out.println("executing request " + httppost.getRequestLine());

        try (CloseableHttpResponse response = httpclient.execute(httppost)) {
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

            // Get uploaded file name from Header and pass it to the next request using props
            String uploadedFileName = response.getHeaders(Consts.FILE_NAME)[0].getValue();
            props.put(Consts.FILE_NAME, uploadedFileName);

            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {
                System.out.println(EntityUtils.toString(resEntity));
                EntityUtils.consume(resEntity);

                return response.getStatusLine().getStatusCode();
            }
        }

        return HttpStatus.SC_NO_CONTENT;
    }

    /**
     * This method is reusing the same HttpClient as a previous one and it also accepts props that should have an
     * uploaded file name as one of its properties.
     *
     * @param httpclient
     * @param props
     * @return
     * @throws java.io.IOException
     */
    public int processReq(CloseableHttpClient httpclient, Properties props) throws IOException {

        HttpPost httppost = new HttpPost(props.getProperty(Consts.HOST_URL_PROCESS));
        
        // Build the header to pass properties
        httppost.addHeader(Consts.HZ_JSON,          props.getProperty(Consts.HZ_JSON));
        httppost.addHeader(Consts.FILE_NAME,        props.getProperty(Consts.FILE_NAME));
        httppost.addHeader(Consts.CLASSIFIER_LIST,  this.getModelListJson());
        httppost.addHeader(Consts.NUM_OF_FOLDS,     props.getProperty(Consts.NUM_OF_FOLDS));

        System.out.println("Executing request " + httppost.getRequestLine());

        // Create a custom response handler
        ResponseHandler<String> responseHandler = (final HttpResponse response) -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        };

        String responseBody = httpclient.execute(httppost, responseHandler);
        System.out.println("----------------------------------------");
        System.out.println(responseBody);

        return HttpStatus.SC_OK;
    }
    
    /**
     * 
     * @return 
     */
    private String getModelListJson() {
        List<String> list = Arrays.asList("J48", "PART", "DecisionTable");
        return new Gson().toJson(list, List.class);
    }
}
